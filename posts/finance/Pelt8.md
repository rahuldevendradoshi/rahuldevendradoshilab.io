---
title: "Pelt8"
description: "This post tries to uncover how petroleum(gas) prices are calculated in India and how it varies from the United States."
tags: Petroleum | Gas | Pricing
image: "pelt8"
draft: true
---
I am writing to express my interest in the Citizens Banks Commercial Banking Summer Intern -
Treasury Solutions position. Thank you in advance for taking the time to evaluate my candidacy
for this position.

I am currently pursuing a Master of Science in Finance program at Brandeis International
Business School. Citizens Bank's mission and vision align with my values. What motivates me is
working for an organization, like yours, that serves a higher purpose. I am particularly drawn to
the bank's commitment to providing exceptional service to its customers and being an active
member of the communities, it serves. The dynamic and demanding nature of leveraged
finance particularly fascinates me. In order to create deals that satisfy the interests of both
investors and clients while also efficiently managing risk, this sector of finance requires creative
and strategic thinking. I'm enthusiastic to use my financial knowledge and analytical acumen to
promote the expansion of your organization and assist your clients in achieving their financing
objectives.