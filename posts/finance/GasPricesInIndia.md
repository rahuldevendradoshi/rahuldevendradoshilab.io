---
title: "⛽ Petroleum Pricing in India"
description: "This post tries to uncover how petroleum(gas) prices are calculated in India and how it varies from the United States."
tags: Petroleum | Gas | Pricing
image: "gaspricesinindia"
draft: true
---
The annual budget forms a significant role in shaping a nation’s economy because it lays out a framework of how a
country expects to carry out its economic and political affairs. It enables professionals in equity research, portfolio
management and investment banking to take meaningful assumptions and forecast models by stress-testing several
economic variables. However, because the annual budget is of arcane nature, it is very important to break down the
technical aspects so that ordinary residents of the country can comprehend the implications of monetary and fiscal
policies. One of the most popular commodity that affects all of us is energy. This is because of its versatile nature.
Economic growth is inextricably linked to energy and it is the foundation of modern life. It is required for almost all
economic activities. Petroleum, comprising of crude oil and refined petroleum products, is one of the prime sources of
energy in the world. It affects everyone and that’s why it is essential to know about how it is priced. It is very important to
clearly understand the history of petroleum pricing and why there is Market Determined Pricing Mechanism (MDPM)
today. Indian Government initially came out with Administered Pricing Mechanism (APM) in order to determine pricing
of the petroleum.

## Administered Pricing Mechanism (APM) 
The pricing mechanism was based on the concept of retention price, by
which refiners were allowed to retain out of their sale proceeds - cost of crude, refining cost and a reasonable return on
investment. The same mechanism was extended to marketing and distribution companies, which were compensated for
operating costs along with an assured return. It played a significant role in insulating oil producers, refiners and marking
companies from global oil price fluctuations and fulfilled the socioeconomic objectives of the government considerably
but in the process failed to generate adequate incentives for investment in the sector and thus failed miserably to create a
vibrant and globally competitive oil industry. It had serious limitations like:

1. **Sharp increase in demand for petroleum products and increasingly felt need for large investments:** The demand for
petroleum products particularly in the second half of nineties had been increasing at a compound annual growth
rate of about 6% but investments in the industry failed to keep pace with the demand resulting in large imports of
crude and even finished products. Furthermore, crude oil production had been plateauing without discovery of new
exploratory wells. Large imports simply exacerbated the crisis in macroeconomic management, especially the
exchange rate and inflation and hence it was essential to bring down the imports to manageable levels. 

![fifty](/images/finance/gaspricesinindia/1.png)

2. **Difficulties in periodic adjustment of prices:** With the responsibility of fixing the prices of petroleum products, the government, driven
by political prerogatives, simply kept on postponing the decision of hiking the prices that inevitably led to burgeoning oil pool deficit. 
The only long-term solution to this problem was that the government should get out of the responsibility of fixing prices leaving them to market forces.
3. **Need to make available inputs to user industries at competitive prices:** Petroleum products are vital inputs to key
industries. With the opening up of the economy to international competition, the user industries could become
competitive only if the inputs are made available at market determined competitive prices and not at prices fixed
by the government.
4. **Difficulty in administration of APM:** Administration of APM was becoming increasingly difficult with the partial opening up of the sector 
allowing private sector refineries.

## Transition to MDPM
The Government in November 1994 had set up an industry study group to prepare the blueprint of
the deregulation and tariff reform that was required in the oil sector and provide a framework for the development of
Market Determined Pricing Mechanism (MDPM). At present, the prices of fuel are changed on daily basis which is also
termed as dynamic fuel pricing. Fuel rates are revised everyday at 06:00 AM in India. This makes sure that variations of
global oil prices throughout the day are transmitted and reflected to fuel users and dealers. Let us understand few terms before
understanding how the fuel price determination works:
1. **Oil Refineries:** These refineries buy or extract crude like WTI (West Texas Intermediate) Crude, Brent Crude, OPEC
Basket crude, etc. and convert these crudes into petrol, diesel, Aviation turbine fuel, Biofuels, etc. Examples of such
refineries are Reliance Industries, Nayara Energy, Bharat Petroleum, Indian Oil, Shell, ONGC, Saudi Aramco, etc.
2. **Oil Marketing Companies(OMC):** These are the companies who market the converted crudes (petrol, diesel, etc.) to
dealers and ultimately to users. Examples of such refineries are Indian Oil, Bharat Petroleum, Shell, Essar, etc.
3. **Dealers:** Companies/individuals enagaging in the business of buying fuels from OMCs and distributing them to end-users.
4. **End-users:** Companies or individuals that will be purchasing the processed fuels from dealers and using it to power vehicles from cars to airpanes, 
using to it to generate electricity and even heating living spaces.

## Petrol Pricing Formula
Retail Selling Price of Fuel = Price at which Dealers buys from OMCs + Excise Duty (Central Tax levied on certain goods) + Dealer’s Commission + VAT (Value Added Tax) (State Tax which may differ from State to State).

India imports about 80 per cent of its petrol and diesel demand. And Brent crude oil has a major role to play here. At present, the price of crude oil per barrel stands at about $72, which keeps fluctuating every day. Fuel prices in India are revised daily based on the changing crude oil prices globally. As global crude oil prices go up, the import cost also increases. But that's just one reason for the high retail prices. The remainder of the amount is just state and central government taxes. In fact, fuel prices in India are one of the highest taxed in the world.

![fifty](/images/finance/gaspricesinindia/globaltaxes.png)

## Impact of GST on petrol prices in India

GST is the Goods and Service Tax Act that came into effect in the country in July 2017. Petrol, and its related products, is a commodity that has been kept out of the GST regime. Currently, petroleum products such as crude oil, natural gas, LPG, diesel and petrol are not under the ambit of GST and are subject to central excise duty and state specific VAT. The addition of these taxes seems to exceed the cost of petrol and diesel in the country. The levy of central and excise duty increases the cost of petrol.
The below table explains the tax structure levied on petrol in India.

![fifty](/images/finance/gaspricesinindia/delhi.png)

Let’s consider the below example to understand what happens if petrol comes under the ambit of GST.
As on September 2018 in Delhi, an individual has to pay Rs 70.52 to purchase a litre of petrol. If you minus the excise duty (Rs. 21.48) charged on per litre of petrol by the centre, also the 27% VAT charged by the state government (calculated to be Rs 14.96), dealers commission (Rs 3.24), the actual cost of petrol comes to Rs. 30.85 per litre. For example, as of September 2018 in Delhi, an individual has to pay Rs. 70.52 for purchasing 1 litre of petrol. If the excise duty i.e. Rs. 21.48 per litre is taken out and 27% VAT i.e. Rs. 14.96 on price of the petrol, dealer’s commission i.e. Rs.3. 24 is waived off, then the actual cost of petrol comes around to be Rs. 30.85 per litre.

## So what happens if petrol comes under GST ambit?

Even if petrol and diesel are brought under GST, they will have to be taxed at a very high rate. Currently, the highest rate of GST is 28%. Of course, there are surcharges and cess that can be charged by the government over and above this. It can be concluded in view of the above that it is quite impossible as of now to bring petrol and diesel under GST regime in the next five to seven years because states would not be ready for an annual revenue loss of ₹2 lakh crore (collectively by all states).

## Conclusion:

Petroleum prices are evaporative in the short run because demand and supply are inelastic. This is due to the facts that there is a limited supply of oil and petroleum product which means any disruption to supply will shift the supply curve to the left, resulting in a sharp increase in prices. In terms of demand, prices are volatile because at present there are no readily violable substitutes to this natural product, so an increase in demand, such as from developing nations will shift the demand curve to the right also causing a sharp increase in price. Petroleum processed in the short run is therefore very sensitive to changes in demand and increased supply. For example, oil supply may be increased through new extraction technologies or the discovery of new oil fields that will shift the supply curve to the right and reduce oil prices. Demand may be decreased through the development of “green technology” which is man-made technologies such as hydrogen-fuelled cars, thereby sifting the demand curve to the left and lowering the oil prices. Therefore, in long run, petroleum prices will be less volatile because if oil prices get too high people will use a cheaper substitute, and when prices rise, producers will supply more oil. But until and unless there is no substitute to be used as fuel for vehicles the demand and supply of petroleum will be at equilibrium.